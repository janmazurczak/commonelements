//
//  watchOSCommonElements.h
//  watchOSCommonElements
//
//  Created by Jan Mazurczak on 20/08/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//

#import <WatchKit/WatchKit.h>

//! Project version number for watchOSCommonElements.
FOUNDATION_EXPORT double watchOSCommonElementsVersionNumber;

//! Project version string for watchOSCommonElements.
FOUNDATION_EXPORT const unsigned char watchOSCommonElementsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <watchOSCommonElements/PublicHeader.h>


