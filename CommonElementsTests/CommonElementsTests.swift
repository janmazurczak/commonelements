//
//  CommonElementsTests.swift
//  CommonElementsTests
//
//  Created by Jan Mazurczak on 19/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import XCTest
@testable import CommonElements

class CommonElementsTests: XCTestCase {
    
    class UpdatingObject: UpdatesObservable {
        let updatesHandler = UpdatesHandler()
        var value: Int = 0
    }
    
    class UpdatesReceiver {
        var value: Int = 0
    }
    
    func testUpdatesObservation() {
        
        let updating = UpdatingObject()
        let observer1 = UpdatesReceiver()
        let observer2 = UpdatesReceiver()
        var observer3: UpdatesReceiver? = UpdatesReceiver()
        
        updating.observe(by: observer1) { $1.value = $0.value }
        updating.observe(by: observer1) { $1.value = $0.value }
        updating.observe(by: observer1) { $1.value = $0.value }
        
        XCTAssertEqual(updating.updatesHandler.observersCount, 1)
        
        updating.observe(by: observer2) { $1.value = $0.value }
        updating.observe(by: observer3!) { $1.value = $0.value }
        
        XCTAssertEqual(updating.updatesHandler.observersCount, 3)
        
        updating.stopObserving(by: observer2)
        
        XCTAssertEqual(updating.updatesHandler.observersCount, 2)
        
        updating.value = 456
        updating.didUpdate()
        
        XCTAssertEqual(observer1.value, 456)
        XCTAssertEqual(observer2.value, 0)
        XCTAssertEqual(observer3!.value, 456)
        
        observer3 = nil
        
        XCTAssertEqual(updating.updatesHandler.observersCount, 1)
        
    }
    
    
    func testGeneratingRandomStrings() {
        for _ in 0..<500 {
            let s = String.random(25)
            XCTAssertEqual(s.distance(from: s.startIndex, to: s.endIndex), 25)
        }
        for _ in 0..<500 {
            let s = String.random(120)
            XCTAssertEqual(s.distance(from: s.startIndex, to: s.endIndex), 120)
        }
        for _ in 0..<500 {
            let s = "🤗😵😈😡🐳🍑🍾".random(10)
            XCTAssertEqual(s.distance(from: s.startIndex, to: s.endIndex), 10)
        }
        for _ in 0..<500 {
            let s = "👴🏾👱🏽🤘🏻☺️ABCD".random(10)
            XCTAssertEqual(s.distance(from: s.startIndex, to: s.endIndex), 10)
        }
        
        for _ in 0..<5 {
            let s = String.random(10)
            NSLog("%@", s)
        }
        for _ in 0..<5 {
            let s = "🤗😵😈😡🐳🍑🍾".random(5)
            NSLog("%@", s)
        }
        for _ in 0..<5 {
            let s = "👴🏾👱🏽🤘🏻☺️ABCD".random(5)
            NSLog("%@", s)
        }
        
        let s = "".random(10)
        XCTAssertEqual(s, "")
    }
    
    
    let seedString = "abcdefg"
    
    func testGeneratingSeedNumbers() {
        let seedData = seedString.data()
        XCTAssertNotNil(seedData)
        let seed = Seed<UInt16>(data: seedData)
        let seedNumbers: [UInt16] = seed.read(.rawSource(), amount: 99)
        XCTAssertEqual(seedNumbers.count, 99)
    }
    
    
    func testNormalisingRandomNumbers() {
        let someRange = 3..<21
        
        var allElements: Set<Int> = []
        for i in 0..<1000 {
            let normalisedNumber = i.normalised(by: someRange)
            XCTAssertTrue(someRange.contains(normalisedNumber))
            allElements.insert(normalisedNumber)
        }
        for i in someRange {
            XCTAssertTrue(allElements.contains(i))
        }
        XCTAssertEqual(allElements.count, 18) // 3 - 20
    }
    
    
    func testDoubleRandomUniforming() {
        for _ in 0..<10000 {
            let r = Double.unsignedRandom(3.0, 7.0)
            XCTAssertGreaterThanOrEqual(r, 3)
            XCTAssertLessThanOrEqual(r, 7)
        }
    }
    
}
