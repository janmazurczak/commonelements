//
//  DocumentsQuery.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 01/05/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


public protocol DocumentsQueryDelegate: class {
    func documentsQueryDidUpdate(_ query: DocumentsQuery, results: NSOrderedSet, removedResults: NSOrderedSet, addedResults: NSOrderedSet, changedResults: NSOrderedSet)
}


extension NSMetadataItem {
    
    public var modificationDate: Date? { return value(forAttribute: NSMetadataItemFSContentChangeDateKey) as? Date }
    public var displayName: String? { return value(forAttribute: NSMetadataItemDisplayNameKey) as? String }
    public var fileName: String? { return value(forAttribute: NSMetadataItemFSNameKey) as? String }
    public var url: URL? { return value(forAttribute: NSMetadataItemURLKey) as? URL }
    
}


open class DocumentsQuery {
    
    
    open weak var delegate: DocumentsQueryDelegate?
    
    
    open func startQuery() {
        metadataQuery.start()
    }
    
    
    public let metadataQuery = NSMetadataQuery()
    
    
    open var count: Int { return metadataQuery.resultCount }
    
    
    open subscript(index: Int) -> Any { return metadataQuery.result(at: index) }
    
    
    open var sorter: (NSMetadataItem, NSMetadataItem) -> Bool = {
        
        if let l = $0.modificationDate, let r = $1.modificationDate , l != r {
            return l.compare(r) != .orderedAscending
        }
        
        if let l = $0.displayName, let r = $1.displayName , l != r {
            return l < r
        }
        
        if let l = $0.fileName, let r = $1.fileName , l != r {
            return l < r
        }
        
        return true
    }
    
    
    fileprivate let workerQueue: OperationQueue = {
        let workerQueue = OperationQueue()
        workerQueue.name = "DocumentsQuery.workerQueue"
        workerQueue.maxConcurrentOperationCount = 1
        return workerQueue
    }()
    
    
    public init() {
        FileManager.default.url(forUbiquityContainerIdentifier: nil)
        
        metadataQuery.searchScopes = [NSMetadataQueryUbiquitousDocumentsScope]
        metadataQuery.operationQueue = workerQueue
        
        NotificationCenter.default.addObserver(self, selector: #selector(DocumentsQuery.finishGathering(_:)), name: Notification.Name.NSMetadataQueryDidFinishGathering, object: metadataQuery)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DocumentsQuery.queryUpdated(_:)), name: Notification.Name.NSMetadataQueryDidUpdate, object: metadataQuery)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func finishGathering(_ notification: Notification) {
        metadataQuery.disableUpdates()
        let metadataQueryResults = metadataQuery.results as! [NSMetadataItem]
        let results = buildModelObjectSet(metadataQueryResults)
        metadataQuery.enableUpdates()
        
        delegate?.documentsQueryDidUpdate(self, results: results, removedResults: NSOrderedSet(), addedResults: NSOrderedSet(), changedResults: NSOrderedSet())
    }
    
    
    @objc func queryUpdated(_ notification: Notification) {
        let changedMetadataItems = notification.userInfo?[NSMetadataQueryUpdateChangedItemsKey] as? [NSMetadataItem]
        let removedMetadataItems = notification.userInfo?[NSMetadataQueryUpdateRemovedItemsKey] as? [NSMetadataItem]
        let addedMetadataItems = notification.userInfo?[NSMetadataQueryUpdateAddedItemsKey] as? [NSMetadataItem]
        
        let changedResults = buildModelObjectSet(changedMetadataItems ?? [])
        let removedResults = buildModelObjectSet(removedMetadataItems ?? [])
        let addedResults = buildModelObjectSet(addedMetadataItems ?? [])
        
        let newResults = buildQueryResultSet()
        
        delegate?.documentsQueryDidUpdate(self, results: newResults, removedResults: removedResults, addedResults: addedResults, changedResults: changedResults)
    }
    
    
    fileprivate func buildModelObjectSet(_ objects: [NSMetadataItem]) -> NSOrderedSet {
        var array = objects
        array.sort(by: sorter)
        return NSMutableOrderedSet(array: array)
    }
    
    
    fileprivate func buildQueryResultSet() -> NSOrderedSet {
        metadataQuery.disableUpdates()
        let metadataQueryResults = metadataQuery.results as! [NSMetadataItem]
        let results = buildModelObjectSet(metadataQueryResults)
        metadataQuery.enableUpdates()
        
        return results
    }
    
    
}

