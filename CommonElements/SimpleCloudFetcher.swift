//
//  SimpleCloudFetcher.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 28/10/2016.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit


public class SimpleCloudFetcher: CloudFetcher, UpdatesObservable {
    
    
    public enum Update {
        case resetContents
        case recordAdded(CKRecord)
    }
    
    
    public let updatesHandler = UpdatesHandler()
    
    
    public private(set) var database: CKDatabase?
    public private(set) var itemType: String
    public private(set) var cloudPredicate: NSPredicate
    public private(set) var sortDescriptors: [NSSortDescriptor]
    public private(set) var prefetchDesiredKeys: [String]?
    
    public var contents = [CloudRecord]()
    public var runningQuery: CKQueryOperation?
    
    public private(set) var runningOperationQueue: OperationQueue?
    public private(set) var syncOperationQueue: OperationQueue = OperationQueue(maxConcurrentOperationCount: 1)
    public private(set) var finishOperationQueue: OperationQueue?
    
    public private(set) var completion: (([CKRecord]?, Error?) -> Void)?
    
    
    public init(
        database: CKDatabase?,
        runningOperationQueue: OperationQueue? = nil,
        itemType: String,
        predicate: NSPredicate,
        sortDescriptors: [NSSortDescriptor],
        desiredKeys: [String]? = nil,
        finishOperationQueue: OperationQueue? = nil,
        completion: (([CKRecord]?, Error?) -> Void)?)
    {
        self.database = database
        self.runningOperationQueue = runningOperationQueue
        self.itemType = itemType
        self.cloudPredicate = predicate
        self.sortDescriptors = sortDescriptors
        self.prefetchDesiredKeys = desiredKeys
        self.finishOperationQueue = finishOperationQueue
        self.completion = completion
    }
    
    
    open func resetData() {
        didUpdate(with: Update.resetContents)
    }
    
    
    open func added(record: CloudRecord) {
        didUpdate(with: Update.recordAdded(record.prefetchedRecord))
    }
    
    
    open func finishedFetching(error: Error?) {
        if let queue = finishOperationQueue, queue != OperationQueue.current {
            queue.addOperation { self.finishedFetching(error: error) }
            return
        }
        
        if error == nil {
            completion?(contents.map{ $0.prefetchedRecord }, nil)
        } else {
            completion?(nil, error)
        }
    }
    
    
    open func fetch(retainUntilCompleted: Bool) {
        if retainUntilCompleted {
            let originalCompletion = completion
            completion = { results, error in
                _ = self // strong reference in purpose of retaining self until finished
                originalCompletion?(results, error)
            }
        }
        fetch()
    }
    
    
}
