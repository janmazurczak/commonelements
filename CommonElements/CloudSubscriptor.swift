//
//  CloudSubscriptor.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 26/10/2016.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit

#if os(iOS) || os(tvOS)
public protocol CloudSubscriptor {
    
    var itemType: String { get }
    var database: CKDatabase { get }
    var subscriptionID: String? { get }
    var subscriptionOptions: CKQuerySubscription.Options { get }
    /// Make sure subscribingQueue maxConcurrentOperationsCount is 1
    var subscribingQueue: OperationQueue { get set }
    
    func subscriptionNotificationInfo() -> CKSubscription.NotificationInfo?
    func subscribe(predicate: NSPredicate)
    
    func subscriptionUpdatedUnknownRecord()
    func subscription(created recordID: CKRecord.ID, recordFields: [String : Any]?)
    func subscription(updated recordID: CKRecord.ID, recordFields: [String : Any]?)
    func subscription(deleted recordID: CKRecord.ID, recordFields: [String : Any]?)
    
}


public extension CloudSubscriptor {
    
    
    func subscribe(predicate: NSPredicate) {
        guard let subscriptionID = subscriptionID else { return }
        subscribingQueue.cancelAllOperations()
        
        let subscription = CKQuerySubscription(
            recordType: itemType,
            predicate: predicate,
            subscriptionID: subscriptionID,
            options: subscriptionOptions)
        subscription.notificationInfo = subscriptionNotificationInfo()
        
        let operation = CKModifySubscriptionsOperation(subscriptionsToSave: [subscription], subscriptionIDsToDelete: nil)
        operation.database = database
        operation.modifySubscriptionsCompletionBlock = { added, removed, error in
            if let error = error { NSLog("Subscribing error: \(error)") }
            //added?.forEach{ NSLog("Subscribed: \($0)") }
        }
        subscribingQueue.addOperation(operation)
    }
    
    
}


public extension CloudSubscriptor {
    
    
    func update(with queryNotification: CKQueryNotification) {
        guard let subscriptionID = subscriptionID else { return }
        guard queryNotification.subscriptionID == subscriptionID else { return }
        
        if let recordID = queryNotification.recordID {
            switch queryNotification.queryNotificationReason {
            case .recordCreated: subscription(created: recordID, recordFields: queryNotification.recordFields)
            case .recordUpdated: subscription(updated: recordID, recordFields: queryNotification.recordFields)
            case .recordDeleted: subscription(deleted: recordID, recordFields: queryNotification.recordFields)
            @unknown default: subscription(updated: recordID, recordFields: queryNotification.recordFields)
            }
        } else {
            subscriptionUpdatedUnknownRecord()
        }
    }
    
    
}
#endif
