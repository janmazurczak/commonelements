//
//  FilesCache.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 24/12/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


public class FilesCache {
    
    
    public static let directory: URL = createFileCacheDirectory()
    var URLsToClean = [URL]()
    
    
    public init() { }
    
    
    public func addEmptyURL(isDirectory: Bool = false) -> URL {
        let url = FilesCache.directory.appendingPathComponent(UUID().uuidString, isDirectory: false)
        URLsToClean.append(url)
        return url
    }
    
    
    public func addDirectory() -> URL {
        let url = addEmptyURL(isDirectory: true)
        try? FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        return url
    }
    
    
    public func from(_ sourceURL: URL, cachedFilename: String? = nil) -> URL? {
        let url = prepareNewURLForData(cachedFilename: cachedFilename)
        do {
            try FileManager.default.copyItem(at: sourceURL, to: url)
        } catch {
            return nil
        }
        return url
    }
    
    
    public func from(_ data: Data, cachedFilename: String? = nil) -> URL? {
        let url = prepareNewURLForData(cachedFilename: cachedFilename)
        do {
            try data.write(to: url)
        } catch {
            return nil
        }
        return url
    }
    
    
    public func clean() {
        for url in URLsToClean {
            try? FileManager.default.removeItem(at: url)
        }
    }
    
    
    private func prepareNewURLForData(cachedFilename: String?) -> URL {
        if let filename = cachedFilename {
            return addDirectory().appendingPathComponent(filename, isDirectory: false)
        } else {
            return addEmptyURL()
        }
    }
    
    
    private static func createFileCacheDirectory() -> URL {
        guard let cacheDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else { fatalError("Can not find cache directory") }
        let url = cacheDirectory.appendingPathComponent("FilesCache", isDirectory: true)
        
        try? FileManager.default.removeItem(at: url)
        
        do {
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        } catch {
            fatalError("Can not create cache directory")
        }
        
        return url
    }
    
    
}

