//
//  SaveAndLoad.swift
//  Nothing Left
//
//  Created by Jan Mazurczak on 21/11/15.
//  Copyright © 2015 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


public enum SaveLoadError: Error {
    case canNotParse, incompleteData, fileNotFound, incorrectData, canNotWriteFile
}


public protocol SaveAndLoad {
    init(dictionary: [String : Any]) throws
    func dictionary() -> [String : Any]
}


extension SaveAndLoad {
    
    
    public init(url: URL) throws {
        guard let data = try? Data(contentsOf: url) else { throw SaveLoadError.fileNotFound }
        let dictionary = try PropertyListSerialization.propertyList(from: data, options: PropertyListSerialization.MutabilityOptions(), format: nil) as? [String: Any]
        guard let unwrappedDictionary = dictionary else { throw SaveLoadError.canNotParse }
        try self.init(dictionary: unwrappedDictionary)
    }
    
    
    public func save(url: URL) throws {
        let data = try PropertyListSerialization.data(fromPropertyList: dictionary(), format: .xml, options: 0)
        guard (try? data.write(to: url, options: [.atomic])) != nil else { throw SaveLoadError.canNotWriteFile }
    }
    
    
    public init(keysAndValuesPersistentStore: KeysAndValuesPersistentStore, key: String) throws {
        guard let dictionary = keysAndValuesPersistentStore.dictionary(for: key) else { throw SaveLoadError.canNotParse }
        try self.init(dictionary: dictionary)
    }
    
    
    public func save(keysAndValuesPersistentStore: KeysAndValuesPersistentStore, key: String) {
        keysAndValuesPersistentStore.set(dictionary: dictionary(), for: key)
    }
    
    
}
