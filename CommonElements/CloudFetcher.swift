//
//  CloudFetcher.swift
//
//  Created by Jan Mazurczak on 27/10/2016.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit


public class CloudRecord {
    
    
    public let prefetchedRecord: CKRecord
    public var fullRecord: CKRecord?
    
    
    /// Feel free to store additional runtime information you need to make your CloudRecords more useful.
    public var localCache = [String : Any]()
    
    
    public init(prefetchedRecord: CKRecord, fullRecord: CKRecord?) {
        self.prefetchedRecord = prefetchedRecord
        self.fullRecord = fullRecord
    }
    
    
    public func fetch(from database: CKDatabase, completion: @escaping (CKRecord) -> Void) {
        database.fetch(withRecordID: prefetchedRecord.recordID) { [weak self] fullRecord, _ in
            if let record = fullRecord {
                self?.fullRecord = record
                completion(record)
            }
        }
    }
    
    
    public func fetchIfNeeded(from database: CKDatabase, completion: @escaping (CKRecord) -> Void) {
        if let record = fullRecord {
            completion(record)
            return
        } else {
            fetch(from: database, completion: completion)
        }
    }
    
    
}


public protocol CloudFetcher: class, CloudPredicateProducer {
    
    var database: CKDatabase? { get }
    var itemType: String { get }
    var prefetchDesiredKeys: [String]? { get }
    var sortDescriptors: [NSSortDescriptor] { get }
    
    var contents: [CloudRecord] { get set }
    var runningQuery: CKQueryOperation? { get set }
    
    /// Make sure runningOperationQueue is limited to 1 operation at a time.
    var runningOperationQueue: OperationQueue? { get }
    /// Make sure syncOperationQueue is limited to 1 operation at a time.
    var syncOperationQueue: OperationQueue { get }
    var finishOperationQueue: OperationQueue? { get }
    
    func fetch()
    func resetData()
    func added(record: CloudRecord)
    func finishedFetching(error: Error?)
    
}


public extension CloudFetcher {
    
    
    func fetch() {
        
        syncOperationQueue.addOperation {
            self.contents = [CloudRecord]()
            self.resetData()
            
            let query = CKQuery(recordType: self.itemType, predicate: self.cloudPredicate)
            query.sortDescriptors = self.sortDescriptors
            
            self.performQuery(CKQueryOperation(query: query))
        }
        
    }
    
    
    private func performQuery(_ queryOperation: CKQueryOperation) {
        
        runningQuery?.cancel()
        runningQuery = queryOperation
        
        queryOperation.desiredKeys = prefetchDesiredKeys
        queryOperation.recordFetchedBlock = { [weak self] record in
            self?.syncOperationQueue.addOperation { [weak self, weak queryOperation] in
                if self?.runningQuery == queryOperation {
                    let record = CloudRecord(prefetchedRecord: record, fullRecord: nil)
                    self?.contents.append(record)
                    self?.added(record: record)
                }
            }
        }
        
        queryOperation.queryCompletionBlock = { [weak self, weak queryOperation] cursor, error in
            
            if let error = error {
                if let queue = self?.finishOperationQueue {
                    queue.addOperation { self?.finishedFetching(error: error) }
                } else {
                    self?.finishedFetching(error: error)
                }
                return
            }
            
            if self?.runningQuery == queryOperation {
                if let cursor = cursor { //TODO: make paginating an option triggered from outside
                    self?.syncOperationQueue.addOperation {
                        self?.performQuery(CKQueryOperation(cursor: cursor))
                    }
                } else {
                    if let queue = self?.finishOperationQueue {
                        queue.addOperation { self?.finishedFetching(error: nil) }
                    } else {
                        self?.finishedFetching(error: nil)
                    }
                }
            }
            
        }
        
        if let queue = runningOperationQueue {
            queryOperation.database = database
            queue.addOperation(queryOperation)
        } else {
            database?.add(queryOperation)
        }
    }
    
    
    func added(record: CloudRecord) {}
    func finishedFetching(error: Error?) {}
    
    
}


public protocol CloudPredicateProducer {
    var cloudPredicate: NSPredicate { get }
}


extension CloudRecord: Equatable {
    
    public static func ==(lhs: CloudRecord, rhs: CloudRecord) -> Bool {
        return lhs.prefetchedRecord.recordID == rhs.prefetchedRecord.recordID
    }
    
}

