//
//  Random.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 21/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


extension Double {
    
    public static func unsignedRandom(_ min: Double, _ max: Double, precision: Double = 1000) -> Double {
        let maxDelta = max - min
        let delta = Double(arc4random_uniform(UInt32(precision * maxDelta))) / precision
        return min + delta
    }
    
}


extension Array {
    
    
    public func random(_ length: Int) -> [Element] {
        
        let distance = count
        if distance == 0 { return [] }
        
        var result = [Element]()
        for _ in 0..<length {
            let pickedIndex = Index( arc4random_uniform(UInt32(distance)) )
            let picked = self[pickedIndex]
            result.append(picked)
        }
        return result
    }
    
    
}


open class RandomNotRepeatingValuesServer<T> {
    
    
    public init(_ allValues: [T]) {
        self.allValues = allValues
    }
    
    
    private let allValues: [T]
    private var values = [T]()
    
    
    private func refillIfNeeded() {
        if values.isEmpty { values = allValues }
    }
    
    
    open func getOne() -> T {
        refillIfNeeded()
        let index = Int(arc4random_uniform(UInt32(values.count)))
        return values.remove(at: index)
    }
    
}

