//
//  Math.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 07/05/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


public extension CGPoint {
    
    static func *(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
        return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
    }
    
    static func /(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
        return CGPoint(x: lhs.x / rhs, y: lhs.y / rhs)
    }
    
    static func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
    
    static func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
    }
    
    func distanceFrom(_ point: CGPoint) -> CGFloat {
        let vector = point - self
        return sqrt((vector.x * vector.x) + (vector.y * vector.y))
    }
    
    func enumeratePointsAround(
        maxRadius: CGFloat,
        deltaRadius: CGFloat,
        closeDensity: CGFloat,
        farDensity: CGFloat,
        addPoint: (CGPoint) -> Void,
        continueAfterFinishedRing: () -> Bool)
    {
        var r: CGFloat = 0
        while r < maxRadius {
            let density = ((1 - (r / maxRadius)) * (closeDensity - farDensity)) + farDensity
            r += deltaRadius
            var angle: CGFloat = 0
            while angle <= .pi * 2 {
                angle += ((1 / r) / density)
                let point = CGPoint(x: sin(angle) * r, y: cos(angle) * r) + self
                addPoint(point)
            }
            if continueAfterFinishedRing() == false {
                return
            }
        }
    }
    
}


public func triangleCenter(_ p0: CGPoint, _ p1: CGPoint, _ p2: CGPoint) -> CGPoint {
    let a = p1 - p0
    let b = p2 - p0
    let pa = a * 0.5
    let pb = b * 0.5
    
    let la0 = (pb.y - a.y) / (pb.x - a.x)
    let lb0 = a.y - (la0 * a.x)
    let la1 = (pa.y - b.y) / (pa.x - b.x)
    let lb1 = b.y - (la1 * b.x)
    
    let x = (lb1 - lb0) / (la0 - la1)
    let y = (la0 * x) + lb0
    
    return CGPoint(x: x, y: y) + p0
}


public extension CGSize {
    
    static func *(lhs: CGSize, rhs: CGFloat) -> CGSize {
        return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
    }
    
}

