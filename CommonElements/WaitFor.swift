//
//  Created by Jan Mazurczak on 04/03/2018.
//  Copyright © 2018 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


public class Wait<Element> where Element: UpdatesObservable {
    
    @discardableResult
    public init(
        until element: Element,
        fulfil predicate: @escaping (Element) -> Bool,
        timeout: (interval: TimeInterval, action: () -> Void)? = nil,
        perform action: @escaping (Element) -> Void)
    {
        var fulfiled = false
        
        element.observe(by: self, initiate: true) { element, observer in
            // self is intentionally retained in block to keep WaitFor alive until fulfiled
            if predicate(element) {
                fulfiled = true
                action(element)
                element.stopObserving(by: self)
            }
        }
        
        if let timeout = timeout {
            let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(timeout.interval * 1000))
            DispatchQueue.main.asyncAfter(deadline: deadline) { [weak element, weak self] in
                if fulfiled { return }
                guard let `self` = self else { return }
                element?.stopObserving(by: self)
                timeout.action()
            }
        }
        
    }
    
}

