//
//  NormalizingFloats.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 21/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


extension Float {
    
    public func normalisedWithSoftEdges(minLimit: Float, maxLimit: Float) -> Float {
        assert(maxLimit > minLimit, "maxLimit argument must be greater than minLimit")
        let limitedSelf = max(minLimit, min(self, maxLimit))
        let percentage = (limitedSelf - minLimit) / (maxLimit - minLimit)
        return cos(percentage * .pi) * -0.5 + 0.5
    }
    
    
    public func denormalisedFromSoftEdges(minLimit: Float, maxLimit: Float) -> Float {
        assert(maxLimit > minLimit, "maxLimit argument must be greater than minLimit")
        let percentage = acos((self - 0.5) / -0.5) / .pi
        return (percentage * (maxLimit - minLimit)) + minLimit
    }
    
    
    public func normalisedWithSoftEdge(limit: Float) -> Float {
        let limitedSelf = max(0, min(self, limit))
        let percentage = limitedSelf / limit
        return sin(percentage * .pi * 0.5)
    }
    
    
    public func denormalisedFromSoftEdge(limit: Float) -> Float {
        let percentage = asin(max(0.0, min(self, 1.0))) / (.pi * 0.5)
        return percentage * limit
    }
    
}


extension UnsignedInteger where Self: NumberWithMaxValue {
    
    public var normalisedFloat: Float { return Float(UInt64(self)) / Float(UInt64(type(of: self).max)) }
    
}

