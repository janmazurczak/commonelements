//
//  SeedData.swift
//
//  Created by Jan Mazurczak on 20/09/14.
//  Copyright (c) 2014 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


extension Data {
    
    @available(*, unavailable, message: "Use Seed class instead")
    public func enumerateSeedNumbers<T: UnsignedInteger>(numbersNeededPerAction: Int, action: ([T]) -> Bool) {
        var seedNumbers: [T] = []
        var modifier: Int64 = 0
        var moreNeeded = true
        while moreNeeded {
            while seedNumbers.count < numbersNeededPerAction {
                seedNumbers = seedNumbers + (self + modifier).sha256().readValues()
                modifier = modifier + 1
            }
            moreNeeded = action(seedNumbers)
            seedNumbers.removeFirst(numbersNeededPerAction)
        }
    }
    
    @available(*, unavailable, message: "Use Seed class instead")
    public func enumerateSeedNumbers<T: UnsignedInteger>(_ amount: Int, numbersNeededPerAction: Int, action: ([T]) -> Void) {
        
    }
    
    @available(*, unavailable, message: "Use Seed<T>(data: self).read(.rawSource()) instead")
    public func generateSeedNumbers<T: SeedSource>(_ amount: Int) -> [T] {
        return Seed<T>(data: self).read(.rawSource(), amount: amount)
    }
    
    @available(*, unavailable, message: "Use Seed<UInt8>(data: self).read(.bool()) instead")
    public func generateSeedBool(_ amount: Int) -> [Bool] {
        return Seed<UInt8>(data: self).read(.bool(), amount: amount)
    }
    
    @available(*, unavailable, message: "Use Seed<UInt16>(data: self).read(.float0to1()) instead")
    public func generateSeedFloats(_ amount: Int) -> [Float] {
        return Seed<UInt16>(data: self).read(.float0to1(), amount: amount)
    }
    
    @available(*, unavailable, message: "Use Seed<UInt64>(data: self).read(.float0to1()) instead")
    public func generateSeedFloatsWith64BitsPrecision(_ amount: Int) -> [Float] {
        return Seed<UInt64>(data: self).read(.float0to1(), amount: amount)
    }
    
}

