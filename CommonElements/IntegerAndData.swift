//
//  IntegerAndData.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 21/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


extension BinaryInteger {
    
    
    public func normalised<RangeBound: BinaryInteger>(by range: CountableRange<RangeBound>) -> Int {
        return Int(normalisedIntMax(by: range))
    }
    
    
    public func normalisedIntMax<RangeBound: BinaryInteger>(by range: CountableRange<RangeBound>) -> Int64 {
        let source = Int64(self)
        let from = Int64(range.lowerBound)
        let to = Int64(range.upperBound)
        let result = from + (source % (to - from))
        return result
    }
    
    
    /// Remember to respect Endianness - it writes bytes at default order on the platform
    public func data() -> Data {
        var mutableValue = self
        let buffer = UnsafeBufferPointer(start: &mutableValue, count: 1)
        return Data(buffer: buffer)
    }
    
    
}


extension Data {
    
    
    /// Remember to respect Endianness - it writes bytes at default order on the platform
    public func toInteger<T: BinaryInteger>() -> T {
        var value: T = 0
        let buffer = UnsafeMutableBufferPointer(start: &value, count: 1)
        _ = copyBytes(to: buffer)
        return value
    }
    
    
}


/// Remember to respect Endianness - it writes bytes at default order on the platform
public func +<T: BinaryInteger>(leftValue: Data, rightValue: T) -> Data {
    return leftValue + rightValue.data()
}

