//
//  CloudKitNotifications.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 10/04/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit


public extension CKContainer {
    
    
    func readAllNotifications(completion: @escaping ([CKNotification], Error?) -> Void) {
        var readNotifications = [CKNotification]()
        let fetch = CKFetchNotificationChangesOperation(previousServerChangeToken: nil)
        
        fetch.notificationChangedBlock = { readNotifications.append($0) }
        fetch.fetchNotificationChangesCompletionBlock = { _, error in
            completion(readNotifications, error)
        }
        
        add(fetch)
    }
    
    
    func markNotificationsRead(_ notifications: [CKNotification], batchLimit: Int = 300, completion: ((Error?) -> Void)?) {
        if notifications.count == 0 { completion?(nil); return }
        
        let notificationIDs = notifications.prefix(batchLimit).compactMap{ $0.notificationID }
        let remainingNotifications = notifications.count > batchLimit ? Array(notifications.suffix(from: batchLimit)) : nil
        
        let markRead = CKMarkNotificationsReadOperation(notificationIDsToMarkRead: notificationIDs)
        markRead.markNotificationsReadCompletionBlock = { _, error in
            
            if let code = (error as? CKError)?.code, code == .limitExceeded, batchLimit > 9 {
                self.markNotificationsRead(notifications, batchLimit: batchLimit / 2, completion: completion)
                
            } else if let error = error {
                completion?(error)
                
            } else if let remainingNotifications = remainingNotifications {
                self.markNotificationsRead(remainingNotifications, completion: completion)
                
            } else {
                completion?(nil)
            }
        }
        
        add(markRead)
    }
    
    
    func markAllNotificationsRead(completion: ((Error?) -> Void)?) {
        readAllNotifications { [weak self] readNotifications, error in
            if let error = error { completion?(error); return }
            self?.markNotificationsRead(readNotifications, completion: completion)
        }
    }
    
    
}


