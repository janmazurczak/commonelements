//
//  UpdatesObservation.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 19/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


/// Adopt UpdatesObservable where you want to send updates to multiple (weakly referenced) observers.
/// You need to add let updatesHandler = UpdatesObservable() to adopt protocol.
/// Add observers and call didUpdate as needed.
public protocol UpdatesObservable: class {
    var updatesHandler: UpdatesHandler { get }
    func didUpdate(with message: Any?)
}


extension UpdatesObservable {
    
    public func didUpdate(with message: Any? = nil) {
        updatesHandler.sendUpdate(from: self, with: message)
    }
    
    public func observe<Observer, Message>(
        by observer: Observer,
        for message: Message.Type,
        queue: OperationQueue? = nil,
        initialMessage: Message? = nil,
        action: @escaping (Self, Observer, Message) -> Void)
        where Observer: AnyObject
    {
        let updatesObserver = UpdatesObserverLookingForMessage<Self, Observer, Message>(
            observer: observer,
            queue: queue,
            action: action
        )
        updatesHandler.add(updatesObserver: updatesObserver)
        if let message = initialMessage { updatesObserver.updateReceived(from: self, with: message) }
    }
    
    public func observe<Observer, Message>(
        by observer: Observer,
        forOptional message: Message.Type,
        queue: OperationQueue? = nil,
        initiate: Bool = false,
        initialMessage: Message? = nil,
        action: @escaping (Self, Observer, Message?) -> Void)
        where Observer: AnyObject
    {
        let updatesObserver = UpdatesObserverLookingForOptionalMessage<Self, Observer, Message>(
            observer: observer,
            queue: queue,
            action: action
        )
        updatesHandler.add(updatesObserver: updatesObserver)
        if initiate { updatesObserver.updateReceived(from: self, with: initialMessage) }
    }
    
    public func observe<Observer>(
        by observer: Observer,
        queue: OperationQueue? = nil,
        initiate: Bool = false,
        action: @escaping (Self, Observer) -> Void)
        where Observer: AnyObject
    {
        let updatesObserver = UpdatesObserverIgnoringMessage<Self, Observer>(
            observer: observer,
            queue: queue,
            action: action
        )
        updatesHandler.add(updatesObserver: updatesObserver)
        if initiate { updatesObserver.updateReceived(from: self, with: nil) }
    }
    
    public func stopObserving(by observer: AnyObject) {
        updatesHandler.remove(updatesObserver: observer)
    }
}


// -------------------------------------------------------------


public final class UpdatesHandler {
    private let syncQueue: OperationQueue
    internal var observers: [AnyUpdatesObserver]
    
    public var observersCount: Int {
        var count: Int = 0
        syncQueue.addOperation {
            self.dropEmptyObserverReferences()
            count = self.observers.count
        }
        syncQueue.waitUntilAllOperationsAreFinished()
        return count
    }
    
    public init() {
        syncQueue = OperationQueue(maxConcurrentOperationCount: 1)
        observers = []
    }
    
    private func dropObservers(like observer: AnyUpdatesObserver) {
        observers = observers.filter { $0.typelessObserver !== observer.typelessObserver || type(of: $0) != type(of: observer) }
    }
    
    private func drop(observer: AnyObject?) {
        observers = observers.filter { $0.typelessObserver !== observer }
    }
    
    private func dropEmptyObserverReferences() {
        observers = observers.filter { $0.typelessObserver != nil }
    }
    
    internal func add(updatesObserver: AnyUpdatesObserver) {
        syncQueue.addOperation {
            self.dropEmptyObserverReferences()
            self.dropObservers(like: updatesObserver)
            self.observers.append(updatesObserver)
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
    internal func remove(updatesObserver: AnyObject) {
        syncQueue.addOperation {
            self.dropEmptyObserverReferences()
            self.drop(observer: updatesObserver)
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
    internal func sendUpdate(from sender: Any, with message: Any?) {
        var observers = [AnyUpdatesObserver]()
        syncQueue.addOperation {
            self.dropEmptyObserverReferences()
            observers = self.observers
        }
        syncQueue.waitUntilAllOperationsAreFinished()
        
        for observer in observers {
            observer.updateReceived(from: sender, with: message)
        }
    }
    
}


internal protocol AnyUpdatesObserver {
    var typelessObserver: AnyObject? { get }
    func updateReceived(from updatedObject: Any, with message: Any?)
}


private struct UpdatesObserverLookingForMessage<Observable, Observer, Message>: AnyUpdatesObserver where Observer: AnyObject {
    
    weak var observer: Observer?
    weak var queue: OperationQueue?
    let action: (Observable, Observer, Message) -> Void
    
    internal var typelessObserver: AnyObject? { return observer }
    
    internal func updateReceived(from updatedObject: Any, with message: Any?) {
        guard let observer = observer else { return }
        guard let observable = updatedObject as? Observable else { return }
        
        if let message = message as? Message {
            queue.performIfCurrentOrNilOtherwiseAdd {
                self.action(observable, observer, message)
            }
        }
    }
    
}


private struct UpdatesObserverLookingForOptionalMessage<Observable, Observer, Message>: AnyUpdatesObserver where Observer: AnyObject {
    
    weak var observer: Observer?
    weak var queue: OperationQueue?
    let action: (Observable, Observer, Message?) -> Void
    
    internal var typelessObserver: AnyObject? { return observer }
    
    internal func updateReceived(from updatedObject: Any, with message: Any?) {
        guard let observer = observer else { return }
        guard let observable = updatedObject as? Observable else { return }
        
        if let message = message as? Message {
            queue.performIfCurrentOrNilOtherwiseAdd {
                self.action(observable, observer, message)
            }
        } else if message == nil {
            queue.performIfCurrentOrNilOtherwiseAdd {
                self.action(observable, observer, nil)
            }
        }
    }
    
}


private struct UpdatesObserverIgnoringMessage<Observable, Observer>: AnyUpdatesObserver where Observer: AnyObject {
    
    weak var observer: Observer?
    weak var queue: OperationQueue?
    let action: (Observable, Observer) -> Void
    
    internal var typelessObserver: AnyObject? { return observer }
    
    internal func updateReceived(from updatedObject: Any, with message: Any?) {
        guard let observer = observer else { return }
        guard let observable = updatedObject as? Observable else { return }
        
        queue.performIfCurrentOrNilOtherwiseAdd {
            self.action(observable, observer)
        }
    }
    
}

