//
//  CloudKitHelpers.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 25/06/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit


public extension CKDatabase {
    
    
    func update(recordIDs: [CKRecord.ID], in queue: OperationQueue, perRecordAction: @escaping (CKRecord) -> Void, recordIDsToDelete: [CKRecord.ID]? = nil, atomically: Bool = true, completion: @escaping ([CKRecord]?, Error?) -> Void) {
        
        var records = [CKRecord]()
        
        let fetchOperation = CKFetchRecordsOperation(recordIDs: recordIDs)
        fetchOperation.database = self
        fetchOperation.perRecordCompletionBlock = { (fetchedRecord, _, error) in
            CloudKitErrorHandler.shared.handle(error: error)
            if let record = fetchedRecord {
                perRecordAction(record)
                records.append(record)
            }
        }
        
        let saveOperation = CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete: recordIDsToDelete)
        saveOperation.database = self
        saveOperation.isAtomic = atomically
        saveOperation.modifyRecordsCompletionBlock = { (savedRecords, nil, error) in
            completion(savedRecords, error)
        }
        
        let appendingRecordsToSaveOperation = BlockOperation {
            saveOperation.recordsToSave = records
        }
        
        saveOperation.addDependency(appendingRecordsToSaveOperation)
        appendingRecordsToSaveOperation.addDependency(fetchOperation)
        
        queue.addOperations([fetchOperation, appendingRecordsToSaveOperation, saveOperation], waitUntilFinished: false)
    }
    
    
}
