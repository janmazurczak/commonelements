//
//  CloudNotificationsManager.swift
//
//  Created by Jan Mazurczak on 26/10/2016.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit


public class CloudNotificationManager: UpdatesObservable {
    
    
    public let updatesHandler = UpdatesHandler()
    
    
    public static let shared = CloudNotificationManager()
    
    
    public func handle(remoteNotification userInfo: [AnyHashable : Any], markReadIn container: CKContainer?) {
        
        guard let userInfo = userInfo as? [String : NSObject] else { return }
        let notification = CKNotification(fromRemoteNotificationDictionary: userInfo)!
        
        handle(remoteNotification: notification, markReadIn: container)
        
    }
    
    
    public func handle(remoteNotification notification: CKNotification, markReadIn container: CKContainer?) {
        
        guard
            notification.notificationType == .query,
            let queryNotification = notification as? CKQueryNotification else { return }
        didUpdate(with: queryNotification)
        
        if
            let container = container,
            let notificationID = queryNotification.notificationID
        {
            let markRead = CKMarkNotificationsReadOperation(notificationIDsToMarkRead: [notificationID])
            container.add(markRead)
        }
        
    }
    
    
}
