//
//  TextFileLogger.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 07/04/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


open class TextFileLogger {
    
    
    public static let session = TextFileLogger()
    
    
    static private let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    
    
    static public let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    
    private let queue = OperationQueue(maxConcurrentOperationCount: 1)
    private(set) var text: String
    public let url: URL?
    
    
    public var isAutosaveEnabled: Bool = true
    
    
    public init() {
        let now = Date()
        let timestamp = TextFileLogger.dateFormatter.string(from: now)
        let filename = timestamp + " " + String.random(16)
        text = filename + "\n"
        url = TextFileLogger.documentsURL?.appendingPathComponent(filename + ".txt")
    }
    
    
    open func add(log: String) {
        queue.addOperation {
            let now = Date()
            let timestamp = TextFileLogger.dateFormatter.string(from: now)
            self.text.append(timestamp + ": " + log + "\n")
            if self.isAutosaveEnabled { self.save() }
        }
    }
    
    
    open func save() {
        guard let url = url else { return }
        _ = try? text.write(to: url, atomically: true, encoding: .utf8)
    }
    
    
}




