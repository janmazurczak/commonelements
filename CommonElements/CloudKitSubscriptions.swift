//
//  CloudKitSubscriptions.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 10/04/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit


public extension CKDatabase {
    
    #if os(iOS) || os(tvOS)
    func removeAllSubscriptions(completion: ((Error?) -> Void)?) {
        let fetch = CKFetchSubscriptionsOperation.fetchAllSubscriptionsOperation()
        
        fetch.fetchSubscriptionCompletionBlock = { [weak self] subscriptions, fetchError in
            guard let subscriptions = subscriptions else { completion?(fetchError); return }
            if subscriptions.count == 0 { completion?(fetchError); return }
            let delete = CKModifySubscriptionsOperation(subscriptionsToSave: nil, subscriptionIDsToDelete: subscriptions.map{ $0.value.subscriptionID })
            delete.modifySubscriptionsCompletionBlock = { _, _, modifyError in
                completion?(modifyError)
            }
            self?.add(delete)
        }
        
        add(fetch)
    }
    #endif
    
    
}
