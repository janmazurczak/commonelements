//
//  Created by Jan Mazurczak on 30/06/2018.
//  Copyright © 2018 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import UIKit


public class Delay {
    
    public enum State {
        case waiting, delivering, delivered, cancelled
    }
    
    #if os(iOS) || os(tvOS)
    
    private var backgroudTaskID: UIBackgroundTaskIdentifier?
    private weak var application: UIApplication?
    
    @discardableResult
    public init(
        by timeInterval: TimeInterval,
        in queue: DispatchQueue = .main,
        deliverInBackground application: UIApplication? = nil,
        action: @escaping () -> Void)
    {
        self.action = action
        
        let time: TimeInterval
        if let application = application {
            self.application = application
            self.backgroudTaskID = application.beginBackgroundTask{ self.deliver() }
            time = max(0, min(application.backgroundTimeRemaining - 0.1, timeInterval))
            NSLog("background task began (\(time)s)")
        } else {
            time = timeInterval
        }
        
        let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(time * 1000))
        queue.asyncAfter(deadline: deadline) {
            // self is intentionally retained in block to keep Delay alive until fulfiled
            self.deliver()
        }
    }
    
    #else
    
    @discardableResult
    public init(
        by timeInterval: TimeInterval,
        in queue: DispatchQueue = .main,
        action: @escaping () -> Void)
    {
        self.action = action
        let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(timeInterval * 1000))
        queue.asyncAfter(deadline: deadline) {
            // self is intentionally retained in block to keep Delay alive until fulfiled
            self.deliver()
        }
    }
    
    #endif
    
    private var action: (() -> Void)?
    
    private var realState: State = .waiting {
        didSet {
            switch realState {
            case .delivering:
                if let action = action {
                    action()
                    self.action = nil
                    realState = .delivered
                } else {
                    realState = .cancelled
                }
            case .delivered, .cancelled:
                #if os(iOS) || os(tvOS)
                if let taskID = backgroudTaskID {
                    application?.endBackgroundTask(taskID)
                    backgroudTaskID = nil
                    NSLog((realState == .delivered ? "finished" : "cancelled") + " background task")
                }
                #else
                break
                #endif
            default:
                break
            }
        }
    }
    
    // Using var setter and getter to handle delivering guarantees thread safety
    public private(set) var state: State {
        get {
            return realState
        }
        set {
            switch (realState, newValue) {
            case (.waiting, .delivering), (.waiting, .cancelled):
                realState = newValue
            default:
                break
            }
        }
    }
    
    public func deliver() {
        state = .delivering
    }
    
    public func cancel() {
        state = .cancelled
    }
    
}

