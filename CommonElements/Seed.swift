
// copyright: Jan Mazurczak 2014-2018
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import Foundation


public typealias SeedSource = UnsignedInteger & NumberWithMaxValue


public class Seed<Source: SeedSource> {
    
    public let data: Data
    public init(data: Data) { self.data = data }
    public init(string: String) { self.data = string.data() }
    
    private var sourceNumbers = [Source]()
    private var sourceModifier: Int64 = 0
    public var precachedNumbersCount: Int { return sourceNumbers.count }
    public private(set) var utilisedNumbersCount: Int = 0
    
    private func refill() {
        sourceNumbers = sourceNumbers + (data + sourceModifier).sha256().readValues()
        sourceModifier += 1
    }
    
    var numberOfValuesInOneRead: Int {
        let elementSize = MemoryLayout<Source>.size
        return 32 / elementSize // 32 stands for length of sha256 in bytes
    }
    
    open func read<Output>(_ reader: SeedReader<Source, Output>) -> Output {
        while sourceNumbers.count < reader.numbersNeeded {
            refill()
        }
        let source = Array(sourceNumbers.prefix(upTo: reader.numbersNeeded))
        sourceNumbers.removeFirst(reader.numbersNeeded)
        utilisedNumbersCount += reader.numbersNeeded
        
        let result = reader.outputConverter(source)
        return result
    }
    
    open func read<Output>(_ reader: SeedReader<Source, Output>, amount: Int) -> [Output] {
        var results = [Output]()
        while results.count < amount {
            results.append(read(reader))
        }
        return results
    }
    
    open func reset() {
        sourceNumbers = []
        sourceModifier = 0
        utilisedNumbersCount = 0
    }
    
    open func restore(utilisedNumbersCount: Int) {
        reset()
        
        sourceModifier = Int64(utilisedNumbersCount / numberOfValuesInOneRead)
        
        let toUtilise = utilisedNumbersCount % numberOfValuesInOneRead
        if toUtilise > 0 {
            refill()
            sourceNumbers.removeFirst(toUtilise)
        }
        self.utilisedNumbersCount = utilisedNumbersCount
    }
    
}


public struct SeedReader<Source: SeedSource, Output> {
    public let numbersNeeded: Int
    public let outputConverter: ([Source]) -> Output
    public init(numbersNeeded: Int, outputConverter: @escaping ([Source]) -> Output) {
        self.numbersNeeded = numbersNeeded
        self.outputConverter = outputConverter
    }
    
    public func applying<ModifiedOutput>(modificator: @escaping (Output) -> ModifiedOutput) -> SeedReader<Source, ModifiedOutput> {
        let originalConverter = outputConverter
        return SeedReader<Source, ModifiedOutput>(numbersNeeded: numbersNeeded) {
            let originalOutput = originalConverter($0)
            return modificator(originalOutput)
        }
    }
}

extension SeedReader where Source == Output {
    public static func rawSource() -> SeedReader {
        return SeedReader(numbersNeeded: 1) {
            return $0[0]
        }
    }
}

extension SeedReader where Output == Bool {
    public static func bool() -> SeedReader {
        return SeedReader(numbersNeeded: 1) {
            return $0[0] % 2 == 0
        }
    }
}

extension SeedReader where Output == Float {
    public static func float0to1Converter() -> (([Source]) -> Float) {
        let max = Float(UInt64(Source.max))
        return {
            let value = $0[0]
            return Float(UInt64(value)) / max
        }
    }
    
    public static func float0to1() -> SeedReader {
        return SeedReader(numbersNeeded: 1, outputConverter: float0to1Converter())
    }
}


extension Data {
    
    public func readValues<T: BinaryInteger>() -> [T] {
        let elementSize = MemoryLayout<T>.size
        let numberOfElements = count / elementSize
        guard numberOfElements > 0 else { return [] }
        
        var result: [T] = []
        for i in 0..<numberOfElements {
            let elementData = subdata(in: (i * elementSize)..<((i + 1) * elementSize))
            result.append(elementData.toInteger())
        }
        return result
    }
    
}


public protocol NumberWithMaxValue {
    static var max: Self { get }
}


extension UInt: NumberWithMaxValue {}
extension UInt8: NumberWithMaxValue {}
extension UInt16: NumberWithMaxValue {}
extension UInt32: NumberWithMaxValue {}
extension UInt64: NumberWithMaxValue {}

