//
//  KeysAndValuesPersistentStore.swift
//  Game
//
//  Created by Jan Mazurczak on 17/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


public protocol KeysAndValuesPersistentStore {
    
    func allKeysAndValues() -> [String : Any]
    
    func set(data: Data?, for key: String)
    
    func dictionary(for key: String) -> [String : Any]?
    func set(dictionary: [String : Any]?, for key: String)
    
    func removeValue(for key: String)
    
    var externalChangeNotificationName: NSNotification.Name? { get }
    
}


extension UserDefaults: KeysAndValuesPersistentStore {
    
    public func set(data: Data?, for key: String) {
        set(data, forKey: key)
    }
    
    
    public func set(dictionary: [String : Any]?, for key: String) {
        set(dictionary, forKey: key)
    }
    
    
    public func dictionary(for key: String) -> [String : Any]? {
        return dictionary(forKey: key)
    }
    
    
    public func allKeysAndValues() -> [String : Any] {
        return dictionaryRepresentation()
    }
    
    
    public func removeValue(for key: String) {
        removeObject(forKey: key)
    }
    
    
    public var externalChangeNotificationName: NSNotification.Name? { return nil }
    
}


#if os(iOS) || os(tvOS)
extension NSUbiquitousKeyValueStore: KeysAndValuesPersistentStore {
    
    
    public func set(data: Data?, for key: String) {
        set(data, forKey: key)
    }
    
    
    public func set(dictionary: [String : Any]?, for key: String) {
        set(dictionary, forKey: key)
    }
    
    
    public func dictionary(for key: String) -> [String : Any]? {
        return dictionary(forKey: key)
    }
    
    
    public func allKeysAndValues() -> [String : Any] {
        return dictionaryRepresentation
    }
    
    
    public func removeValue(for key: String) {
        removeObject(forKey: key)
    }
    
    
    public var externalChangeNotificationName: NSNotification.Name? { return NSUbiquitousKeyValueStore.didChangeExternallyNotification }
    
    
}
#endif

