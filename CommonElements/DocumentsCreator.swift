//
//  DocumentsCreator.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 03/05/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


open class DocumentsCreator {
    
    
    public static let shared = DocumentsCreator()
    
    
    fileprivate let coordinationQueue: OperationQueue = {
        let coordinationQueue = OperationQueue()
        coordinationQueue.name = "DocumentsCreator.coordinationQueue"
        return coordinationQueue
    }()
    
    
    public enum CreatingError: Error {
        case cloudNotEnabled
    }
    
    
    open func createNewDocumentWithTemplate(_ templateURL: URL, ext: String, completion: ((URL?, Error?) -> Void)?) {
        
        let completionInMainThread = { (url: URL?, error: Error?) in
            OperationQueue.main.addOperation { completion?(url, error) }
        }
        
        coordinationQueue.addOperation {
            let fileManager = FileManager()
            guard let documentsURL = fileManager.cloudDocumentsURL() else {
                completionInMainThread(nil, CreatingError.cloudNotEnabled)
                return
            }
            
            let targetURL = documentsURL.newDocumentURL(ext: ext)
            
            self.copyDocument(sourceURL: templateURL, targetURL: targetURL, fileManager: fileManager, completion: completionInMainThread)
        }
    }
    
    
    fileprivate func copyDocument(sourceURL: URL, targetURL: URL, fileManager: FileManager, completion: @escaping ((URL?, Error?) -> Void)) {
        let readIntent = NSFileAccessIntent.readingIntent(with: sourceURL, options: [])
        let writeIntent = NSFileAccessIntent.writingIntent(with: targetURL, options: .forReplacing)
        
        NSFileCoordinator().coordinate(with: [readIntent, writeIntent], queue: self.coordinationQueue) { error in
            if let error = error { completion(nil, error); return }
            
            do {
                try fileManager.copyItem(at: readIntent.url, to: writeIntent.url)
                completion(writeIntent.url, nil)
            } catch {
                completion(nil, error)
            }
        }
    }
    
    
}


extension URL {
    
    
    fileprivate func newDocumentURL(name: String = "Untitled", ext: String) -> URL {
        
        var target = appendingPathComponent(name).appendingPathExtension(ext)
        
        var nameSuffix = 1
        while (try? target.checkPromisedItemIsReachable()) ?? false {
            nameSuffix += 1
            target = appendingPathComponent(name + "-\(nameSuffix)").appendingPathExtension(ext)
        }
        
        return target
    }
    
    
}


extension FileManager {
    
    
    fileprivate func cloudDocumentsURL() -> URL? {
        return url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents")
    }
    
    
}
