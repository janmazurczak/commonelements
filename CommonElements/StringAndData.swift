//
//  StringAndData.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 21/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


extension Data {
    
    
    public func hexString() -> String {
        var result: String = ""
        for i in 0..<count {
            let byte: UInt8 = self[i]
            result = result + String(format: "%02x", byte)
        }
        return result
    }
    
    
}


extension String {
    
    public func data() -> Data {
        return self.data(using: String.Encoding.utf8)!
    }
    
    
    public static func randomAllowedCharacters() -> String {
        return "abcdefghijklmnoprquwvxyzABCDEFGHIJKLMNOPRQUWVXYZ1234567890"
    }
    
    
    public static func randomAllowedCharacterSet() -> CharacterSet {
        return CharacterSet(charactersIn: randomAllowedCharacters())
    }
    
    
    public static func random(_ length: Int) -> String {
        return randomAllowedCharacters().random(length)
    }
    
    
    public func random(_ length: Int) -> String {
        let allCharacters = Array(self)
        let randomCharacters = allCharacters.random(length)
        return String(randomCharacters)
    }
    
    
    var dataFromHex: Data? {
        var data = Data(capacity: count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        
        return data
    }

    
}


public func +(leftValue: Data, rightValue: String) -> Data {
    return leftValue + rightValue.data(using: String.Encoding.utf8)!
}


