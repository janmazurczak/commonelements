//
//  Shop.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 23/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#if os(iOS) || os(tvOS)
import StoreKit


public protocol Buyer: class {
    func shopDidDiscover(availableProduct: SKProduct)
    func shopDidBuy(_ shop: Shop, productID: String, quantity: Int, transactionIdentifier: String?, restored: Bool) -> Bool
    func shopDidFailBuying(_ shop: Shop, productID: String)
}


open class Shop: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    
    public let offeredProductIDs: Set<String>
    open weak var buyer: Buyer? {
        didSet {
            processPendingTransactions()
        }
    }
    
    open fileprivate(set) var availableProducts = [String : SKProduct]()
    open fileprivate(set) var productsWaitingToBeBoughtWhenAvailable = [String : Int]()
    
    
    public init(productIDs: Set<String>, buyer: Buyer? = nil) {
        self.offeredProductIDs = productIDs
        self.buyer = buyer
        super.init()
        SKPaymentQueue.default().add(self)
        requestForProducts()
        processPendingTransactions()
    }
    
    
    deinit {
        SKPaymentQueue.default().remove(self)
    }
    
    
    open func restore() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    
    open func buy(_ productID: String, quantity: Int = 1) {
        if let product = availableProducts[productID] {
            product.buy(quantity: quantity)
        } else {
            productsWaitingToBeBoughtWhenAvailable[productID] = quantity
        }
    }
    
    
    var lastProductsRequest: SKProductsRequest?
    public func requestForProducts() {
        lastProductsRequest = SKProductsRequest(productIdentifiers: offeredProductIDs)
        lastProductsRequest?.delegate = self
        lastProductsRequest?.start()
    }
    
    
    open func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        response.products.forEach {
            if offeredProductIDs.contains($0.productIdentifier) {
                availableProducts[$0.productIdentifier] = $0
                buyer?.shopDidDiscover(availableProduct: $0)
                buyProductIfWaitingForAvailability($0)
            }
        }
    }
    
    
    fileprivate func buyProductIfWaitingForAvailability(_ product: SKProduct) {
        if let quantity = productsWaitingToBeBoughtWhenAvailable[product.productIdentifier] {
            product.buy(quantity: quantity)
            productsWaitingToBeBoughtWhenAvailable.removeValue(forKey: product.productIdentifier)
        }
    }
    
    
    open func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        processTransactions(transactions)
    }
    
    
    fileprivate func processPendingTransactions() {
        processTransactions(SKPaymentQueue.default().transactions)
    }
    
    
    fileprivate func processTransactions(_ transactions: [SKPaymentTransaction]) {
        transactions.forEach {
            switch $0.transactionState {
            case .purchased, .restored:
                processSuccessfulTransaction($0)
            case .failed:
                processFailedTransaction($0)
            default:
                NSLog("transaction is processing")
            }
        }
    }
    
    
    fileprivate func processSuccessfulTransaction(_ transaction: SKPaymentTransaction) {
        guard offeredProductIDs.contains(transaction.payment.productIdentifier) else { return }
        guard let buyer = buyer else { return }
        
        if buyer.shopDidBuy(
            self,
            productID: transaction.payment.productIdentifier,
            quantity: transaction.payment.quantity,
            transactionIdentifier: transaction.transactionIdentifier,
            restored: transaction.transactionState == .restored)
        {
            SKPaymentQueue.default().finishTransaction(transaction)
        }
    }
    
    
    fileprivate func processFailedTransaction(_ transaction: SKPaymentTransaction) {
        guard offeredProductIDs.contains(transaction.payment.productIdentifier) else { return }
        
        SKPaymentQueue.default().finishTransaction(transaction)
        buyer?.shopDidFailBuying(self, productID: transaction.payment.productIdentifier)
    }
    
    
    open class func Receipt() -> Data? {
        guard let url = Bundle.main.appStoreReceiptURL else { return nil }
        return try? Data(contentsOf: url, options: [])
    }
    
    
    open class func ReceiptJSONData(transactionIdentifier: String?) -> Data? {
        guard let appReceipt = Receipt() else { return nil }
        guard let transactionIdentifier = transactionIdentifier else { return nil }
        
        let receipt: [String : String] = [
            "transaction-identifier" : transactionIdentifier,
            "receipt-data" : appReceipt.base64EncodedString(options: [])
        ]
        
        return try? JSONSerialization.data(withJSONObject: receipt, options: [])
    }
    
}


extension SKProduct {
    
    public func buy(quantity: Int = 1) {
        let payment = SKMutablePayment(product: self)
        payment.quantity = quantity
        SKPaymentQueue.default().add(payment)
    }
    
}
#endif
