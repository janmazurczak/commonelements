//
//  CloudKitLogHandler.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 15/06/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CloudKit


open class CloudKitErrorHandler: UpdatesObservable {
    
    
    public static let shared = CloudKitErrorHandler()
    public let updatesHandler = UpdatesHandler()
    
    
    public var sameErrorMinimumTimeInterval: TimeInterval = 3
    
    
    private(set) var lastErrorCode: CKError.Code?
    private(set) var lastErrorTime: DispatchTime?
    
    
    private func handle(_ logEntry: CKError) {
        
        let time = DispatchTime.now()
        
        if
            logEntry.code == lastErrorCode,
            let lastTime = lastErrorTime,
            time < lastTime + DispatchTimeInterval.milliseconds(Int(sameErrorMinimumTimeInterval * 1000))
        {
            return
        }
        
        OperationQueue.main.addOperation {
            self.lastErrorTime = time
            self.lastErrorCode = logEntry.code
            self.didUpdate(with: logEntry)
        }
    }
    
    
    open func handle(error: Error?) {
        guard let error = error as? CKError else { return }
        
        NSLog("\(error.localizedDescription)")
        
        handle(error)
    }
    
    
}

