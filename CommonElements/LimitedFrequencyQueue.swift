//
//  LimitedFrequencyQueue.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 08/01/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


open class LimitedFrequencyQueue {
    
    
    let waitingQueue = OperationQueue(maxConcurrentOperationCount: 1)
    let waitingDispatchQueue = DispatchQueue(label: "Waiting in LimitedFrequencyQueue")
    
    open var interval: TimeInterval
    public var executionQueue: OperationQueue?
    
    
    public var operationCount: Int { return waitingQueue.operationCount }
    public var isDelayingNextActionNow: Bool { return waitingQueue.operationCount > 0 || waitingQueue.isSuspended }
    
    
    public init(interval: TimeInterval, executionQueue: OperationQueue?) {
        self.interval = interval
        self.executionQueue = executionQueue
    }
    
    
    private func addingOperation(action: @escaping (OperationQueue?) -> Void) {
        waitingQueue.addOperation {
            let waitingID = UUID()
            self.waitingID = waitingID
            self.waitingQueue.isSuspended = true
            
            action(self.executionQueue)
            
            let unlockTime = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(self.interval * 1000))
            self.waitingEndTime = unlockTime
            self.waitingDispatchQueue.asyncAfter(deadline: unlockTime) {
                guard waitingID == self.waitingID else { return }
                self.cancelWaiting()
            }
        }
    }
    
    
    private var waitingEndTime: DispatchTime?
    
    
    private var waitingID: UUID?
    open func cancelWaiting() {
        waitingID = nil
        waitingEndTime = nil
        waitingQueue.isSuspended = false
    }
    
    
    open func cancelWaitingOperations() {
        waitingQueue.cancelAllOperations()
    }
    
    
    open func addOperation(cancelWaitingOperations cancelWaitingOp: Bool = true, startImmediately: Bool = false, _ block: @escaping () -> Void) {
        if cancelWaitingOp || startImmediately { cancelWaitingOperations() }
        if startImmediately { cancelWaiting() }
        addingOperation { $0?.addOperation(block) }
    }
    
    
    open func addOperation(cancelWaitingOperations cancelWaitingOp: Bool = true, startImmediately: Bool = false, _ op: Operation) {
        if cancelWaitingOp || startImmediately { cancelWaitingOperations() }
        if startImmediately { cancelWaiting() }
        addingOperation { $0?.addOperation(op) }
    }
    
    
}
